# Pi-Hole

## Install
Use docker compose or a [template](https://gitlab.com/livlig-public/docker) to deploy.
See [pi-hole.net](pi-hole.net) for info on Raspberry Pi Install and how to configure your DHCP sever/router.
 
## Post Install
Import Whitelist/Blacklist/Settings via “Teleport” (use a backup or the [generic version ](https://gitlab.com/livlig-public/pi-hole/-/blob/main/pi-hole-generic-teleporter_2021-01-05_18-44-23.tar.gz)in this repo)  

### Ad Lists
Group Management > Ad Lists, can be added all at once, or individually with comments
https://gitlab.com/livlig-public/pi-hole/-/blob/main/ad-lists

### Allow Lists
Script for commonly whitelisted domains for Pi-Hole: https://github.com/anudeepND/whitelist

### Wildcard DNS
Use this to set a default IP to resolve for all subdomains of a domain to a single host. Useful if you have a single VM or conatiner server that you deploy most of your worklaods on.

Pi-Hole does not support local DNS wildcard entries via the GUI. A DNSmasq config file can be added via the CLI. Replace with your domain and IP address.

_NOTE: pi-hole will use the wildcard LAST when resolving. I.E. if router.mydomain.com --> 192.168.0.1, and the wildcard *.mydomain.com --> 192.168.0.10, then fw.mydomain.com will still work correctly._
```
sudo echo address=/mydomain.lab/192.168.0.X >> /etc/dnsmasq.d/02-my-wildcard-dns.conf   
sudo service pihole-FTL restart
```
If using a container: add the file via the host filesystem, or exec, then restart the container.

### Write logs to RAM instead of disk
See https://gitlab.com/livlig-public/documentation/-/wikis/Raspberry-Pi

### Limit log retention and Database Writes
Pi-Hole writes to an SQLite database file every minute. You can set it to write every hour or longer.
You can also force old logs to be deleted.

```
sudo vim /etc/pihole/pihole-FTL.conf
  MAXDBDAYS=7
  MAXLOGAGE=24.0 #logs are deleted after 24h
  DBINTERVAL=60 #limits writes to every 60 minutes
sudo systemctl restart pihole-FTL
```

## Update PiHole
Hardware: `sudo pihole -up`
Docker: same as other containers.
